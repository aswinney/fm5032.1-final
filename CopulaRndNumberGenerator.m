clear
clc

filePath = pwd + "\E&P_AdjClose.csv";
data = csvread(filePath,1,1);
cog = diff(log(data(1:1258,2)));
cxo = diff(log(data(1:1258,3)));
eog = diff(log(data(1:1258,5)));
mro = diff(log(data(1:1258,6)));

CalcPair(cog, mro, 'COG', 'MRO');
CalcPair(cxo, eog, 'CXO', 'EOG');

%FitPair(GetDensity(cog, mro))
%FitPair(GetDensity(cxo, eog))

function [tau, aClayton, aGumbel, aGauss] = CalcPair(stock1, stock2, s1Name, s2Name)
    fig = figure;
    diffStats = @(x) [mean(x(:,1)-x(:,2)) var(x(:,1)-x(:,2))];
    n = 1257;
        
    U = GetDensity(stock1, stock2);
    tau = corr(stock1,stock2,'type','Kendall')
    aClayton = copulaparam('Clayton',tau,'type','kendall')
    aGumbel = copulaparam('Gumbel',tau,'type','kendall')
    aGauss = copulaparam('Gaussian',tau,'type','kendall')
    [t_rho,t_df] = copulafit('t', U,'Method','ApproximateML')
    
    baseStats = diffStats(U)
    subplot(3,3,1)
    plot(U(:,1),U(:,2),'.')
    title(['Base Data'])
    xlabel(s1Name)
    ylabel(s2Name)
    
    claytonRand = copularnd('Clayton',aClayton,n);
    claytonStats = diffStats(claytonRand)
    subplot(3,3,2)
    plot(claytonRand(:,1),claytonRand(:,2),'.');
    title(['Clayton, {\it\alpha} = ',sprintf('%0.2f',aClayton)])
    xlabel(s1Name)
    ylabel(s2Name)
    
    gumbelRand = copularnd('Gumbel',aGumbel,n);
    gumbelStats = diffStats(gumbelRand)
    subplot(3,3,3)
    plot(gumbelRand(:,1),gumbelRand(:,2),'.')
    title(['Gumbel, {\it\alpha} = ',sprintf('%0.2f',aGumbel)])
    xlabel(s1Name)
    ylabel(s2Name)
    
    gaussRand = copularnd('Gaussian',aGauss,n);
    gaussStats = diffStats(gaussRand)
    subplot(3,3,4)
    plot(gaussRand(:,1),gaussRand(:,2),'.')
    title(['Gaussian, {\it\alpha} = ',sprintf('%0.2f',aGauss)])
    xlabel(s1Name)
    ylabel(s2Name)
    
    tRand = copularnd('t',t_rho,t_df,n);
    tStats = diffStats(tRand)
    subplot(3,3,5)
    plot(tRand(:,1),tRand(:,2),'.')
    title(['Student-t, {\it\rho} = ',sprintf('%0.2f',t_rho(1,2))])
    xlabel(s1Name)
    ylabel(s2Name)
    saveas(fig,pwd + "\Figures\" + s1Name +  "_" + s2Name + "_DrawsHist.png")
    csvwrite("GumbelRVData_" + s1Name +  "_" + s2Name + ".csv",copularnd('Gumbel',aGumbel,10000));
end

function [pair] = GetDensity(x, y)
    u = ksdensity(x,x,'function','cdf');
    v = ksdensity(y,y,'function','cdf');
    pair = [u v];
end

function [t_rho, t_df, param_gumbel, param_clayton, param_Gauss] = FitPair(pair)
    [t_rho,t_df] = copulafit('t', pair,'Method','ApproximateML')
    param_gumbel = copulafit('Gumbel', pair)
    param_clayton = copulafit('Clayton', pair)
    param_Gauss = copulafit('Gaussian', pair)
end

